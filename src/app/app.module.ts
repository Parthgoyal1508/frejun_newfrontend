import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule} from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatNativeDateModule} from '@angular/material';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import { AppComponent } from './app.component';
import { NewMeetingComponent } from './new-meeting/new-meeting.component';
import { PricingPlanComponent } from './pricing-plan/pricing-plan.component';
import { DialogOverviewExampleDialog } from './new-meeting/new-meeting.component'

@NgModule({
  declarations: [
    AppComponent,
    NewMeetingComponent,
    PricingPlanComponent,
    DialogOverviewExampleDialog
  ],
  imports: [
    BrowserModule,
    MatCardModule,
    FormsModule,
    HttpClientModule,
    MatNativeDateModule,
    MatDialogModule,
    BrowserAnimationsModule,
    ReactiveFormsModule
  ],
  entryComponents: [NewMeetingComponent, DialogOverviewExampleDialog],
  providers: [],
  bootstrap: [AppComponent, NewMeetingComponent],
  exports : [
    BrowserModule,
    MatCardModule,
    FormsModule,
    HttpClientModule,
    MatNativeDateModule,
    MatDialogModule,
    BrowserAnimationsModule,
    ReactiveFormsModule
  ]
})
export class AppModule {}

platformBrowserDynamic().bootstrapModule(AppModule);
